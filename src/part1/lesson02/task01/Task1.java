package part1.lesson02.task01;

public class Task1 {
    public static void main(String[] args) throws Exception {

        // Моделирование ошибки «NullPointerException»
        String str = null;
        if(str.equals("Hello, World!")) {
            System.out.println(str);
        }

        // Моделирование ошибки «ArrayIndexOutOfBoundsException»
        String[] s = {"Hello, ", "World!"};
        for (int i = 0; i <= s.length; i++) {
            System.out.print(s[i]);
        }

        // Моделирование своего варианта ошибки через оператор throw
        System.out.println("Hello, World!");
        throw new MyException("Exception!");
    }
}
