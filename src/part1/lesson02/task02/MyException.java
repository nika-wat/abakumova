package part1.lesson02.task02;

public class MyException extends Exception{

    public MyException(String message){
        super(message);
    }
}
