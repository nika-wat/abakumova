package part1.lesson02.task02;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) throws Exception {

        System.out.print("Введите число N (количество случайных чисел): ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] k = new int [n];
        for (int i = 0; i < n; i++) {
            k[i] = (int) (Math.random() * 100 * (Math.random() > 0.1 ? 1 : -1));
            if (k[i] < 0) {
                throw new MyException("В последовательности есть отрицательное число!");
            }
            double q = Math.sqrt(k[i]);
            if (k[i] == (int) q * q)
                System.out.print(k[i] + " "); // Если квадрат целой части q числа равен k, выводим число на экран
        }

        // Вывод сгенерированной последовательности чисел
        System.out.println("\nСгенерированная последовательность чисел: ");
        for (int i = 0; i < n; i++) {
            System.out.print(k[i] + " ");
        }
    }
}
