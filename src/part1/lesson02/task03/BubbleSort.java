package part1.lesson02.task03;

import static part1.lesson02.task03.Task3.LENGTH;

public class BubbleSort implements Sort {

    @Override
    public void sorting(Person[] persons) throws Exception {

        // Первые идут мужчины
        boolean needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < LENGTH; i++) {
                if (persons[i].getSex().compareTo(persons[i-1].getSex()) < 0) {
                    swap(persons, i, i-1);
                    needIteration = true;
                }
            }
        }

        // Выше в списке тот, кто более старший
        needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < LENGTH; i++) {
                if ((persons[i].getSex() == persons[i-1].getSex()) &&
                        persons[i].getAge() > persons[i-1].getAge()) {
                    swap(persons, i, i-1);
                    needIteration = true;
                }
            }
        }

        needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < LENGTH; i++) {

                // Если имена людей и возраст совпадают, выбрасываем исключение
                if ((persons[i].getSex() == persons[i-1].getSex()) &&
                        (persons[i].getAge() == persons[i-1].getAge()) &&
                        (persons[i].getName().equals(persons[i-1].getName())))
                    throw new MyException("Имена людей и возраст совпадают!");

                // Имена сортируются по алфавиту
                if ((persons[i].getSex() == persons[i-1].getSex()) &&
                        (persons[i].getAge() == persons[i-1].getAge()) &&
                        ((persons[i].getName().compareTo(persons[i-1].getName()) < 0))) {
                    swap(persons, i, i-1);
                    needIteration = true;
                }
            }
        }
    }
}
