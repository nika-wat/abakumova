package part1.lesson02.task03;

public class MyException extends Exception{

    public MyException(String message){
        super(message);
    }
}
