package part1.lesson02.task03;

import static part1.lesson02.task03.Task3.LENGTH;

public class SelectionSort implements Sort{

    @Override
    public void sorting(Person[] persons) throws Exception{

        // Первые идут мужчины
        for (int left = 0; left < LENGTH; left++) {
            int minInd = left;
            for (int i = left; i < LENGTH; i++) {
                if (persons[i].getSex() == Sex.MAN) {
                    minInd = i;
                }
            }
            swap(persons, left, minInd);
        }

        // Выше в списке тот, кто более старший
        for (int left = 0; left < LENGTH; left++) {
            int minInd = left;
            for (int i = left; i < LENGTH; i++) {

                if ((persons[i].getSex() == persons[minInd].getSex()) &&
                        (persons[i].getAge() > persons[minInd].getAge())) {
                    minInd = i;
                }
            }
            swap(persons, left, minInd);
        }

        // Имена сортируются по алфавиту
        for (int left = 0; left < LENGTH; left++) {
            int minInd = left;
            for (int i = left; i < LENGTH; i++) {
                if ((persons[i].getSex() == persons[minInd].getSex()) &&
                        (persons[i].getAge() == persons[minInd].getAge()) &&
                        ((persons[i].getName().compareTo(persons[minInd].getName()) < 0))) {
                    minInd = i;
                }
            }
            swap(persons, left, minInd);
        }

        // Если имена людей и возраст совпадают, выбрасываем исключение
        for (int i = 1; i < LENGTH; i++) {
            if ((persons[i].getSex() == persons[i - 1].getSex()) &&
                    (persons[i].getAge() == persons[i - 1].getAge()) &&
                    (persons[i].getName().equals(persons[i - 1].getName())))
                throw new MyException("Имена людей и возраст совпадают!");
        }
    }
}
