package part1.lesson02.task03;

public interface Sort {
    void sorting(Person[] persons) throws Exception;

    default void swap(Person[] persons, int left, int minInd) {
        Person tmp = persons[left];
        persons[left] = persons[minInd];
        persons[minInd] = tmp;
    }
}
