package part1.lesson02.task03;

import java.util.Arrays;

public class Task3 {
    public static final int LENGTH = 30; // Длина исходного массива
    public static void main(String[] args) throws Exception{
        Person[] persons = new Person[LENGTH];

        // Генерация исходного массива
        int j = 0;
        for (int i = 0; i < (LENGTH/6); i++) {
            persons[i+j] = new Person((int)(Math.random()*100), Sex.MAN, "Майк");
            persons[i+j+1] = new Person((int)(Math.random()*100), Sex.MAN, "Рауль");
            persons[i+j+2] = new Person((int)(Math.random()*100), Sex.MAN, "Арнольд");
            persons[i+j+3] = new Person((int)(Math.random()*100), Sex.WOMAN, "Джейн");
            persons[i+j+4] = new Person((int)(Math.random()*100), Sex.WOMAN, "Кейт");
            persons[i+j+5] = new Person((int)(Math.random()*100), Sex.WOMAN, "Тоня");
            j+=5;
        }

        long startTime = System.currentTimeMillis(); // Замеряем время работы алгоритма сортировки

//        SelectionSort ss = new SelectionSort(); // Сортировка выбором
//        ss.sorting(persons);

        BubbleSort bs = new BubbleSort(); // Сортировка пузырьком
        bs.sorting(persons);

        long timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("Алгоритм выполнялся " + timeSpent + " миллисекунд(ы)");
        System.out.println(Arrays.toString(persons)); // Вывод на экран отсортированного списка
    }
}
