package part1.lesson02.task03ver2;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {
    public int compare(Person a, Person b) {
        int result = a.getSex().compareTo(b.getSex()); // Сравниваем по полу
        if (result != 0) {
            return result;
        }
        result = b.getAge() - a.getAge(); // Если пол равен, сравниваем по возрасту
        if (result !=0) {
            return result;
        }
        result = a.getName().compareTo(b.getName()); // Если возраст равен, сравниваем по имени
        return result;
    }
}
