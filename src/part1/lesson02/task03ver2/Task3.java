package part1.lesson02.task03ver2;

import java.util.ArrayList;
import java.util.Collections;

// Задание, выполненное с помощью компаратора (оказалось, задание надо выполнять без компаратора)
public class Task3 {
    public static void main(String[] args) {
        ArrayList <Person> persons = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Person mike = new Person ((int)(Math.random()*100), Sex.MAN, "Майк");
            Person raul = new Person ((int)(Math.random()*100), Sex.MAN, "Рауль");
            Person arnold = new Person ((int)(Math.random()*100), Sex.MAN, "Арнольд");
            Person jane = new Person ((int)(Math.random()*100), Sex.WOMAN, "Джейн");
            Person kate = new Person ((int)(Math.random()*100), Sex.WOMAN, "Кейт");
            Person tonia = new Person ((int)(Math.random()*100), Sex.WOMAN, "Тоня");

            persons.add(mike);
            persons.add(raul);
            persons.add(arnold);
            persons.add(jane);
            persons.add(kate);
            persons.add(tonia);
        }
        long startTime = System.currentTimeMillis();
        Collections.sort(persons, new PersonSuperComparator());
        long timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("Алгоритм выполнялся " + timeSpent + " миллисекунд(ы)");
    }
}
