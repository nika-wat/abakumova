package part1.lesson03.task01;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        final int length = 100; // Длина массива Number
        int[] number = new int[length];
        for (int i = 0; i < length; i++) {
            number[i] = (int)(Math.random()*100);
        }
        MathBox mb = new MathBox(number);
        mb.dump();

        mb.addObject(5555);
        System.out.println("Коллекция после добавления элемента 5555:");
        System.out.println(mb.toString());

        mb.deleteObject(1);
        System.out.println("Коллекция после удаления элемента 1:");
        System.out.println(mb.toString());

        int summ = mb.summator();
        System.out.println("Сумма всех элементов коллекции = " + summ);

        Scanner in = new Scanner(System.in);
        System.out.println("Введите делитель:");
        int num = in.nextInt();
        mb.splitter(num);
        System.out.println(mb.toString());

        System.out.println("Введите значение, котрое нужно удалить в коллекции:");
        num = in.nextInt();
        mb.delete(num);
        System.out.println(mb.toString());
    }
}
