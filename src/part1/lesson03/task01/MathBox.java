package part1.lesson03.task01;

import java.util.*;

public class MathBox extends ObjectBox {

    public MathBox(int[] number) {
        super(number);
    }

    // Метод, возвращающий сумму всех элементов коллекции
    public int summator() {
        int summ = 0;
        for (Object o : getSet()) {
            summ += (int) o;
        }
        return summ;
    }

    // Метод, выполняющий поочередное деление всех хранящихся в объекте элементов на делитель
    // Хранящиеся в объекте данные полностью заменяются результатами деления
    public void splitter(Integer num) {
        List list = new ArrayList(getSet());
        getSet().clear();
        int element;
        for (Object o : list) {
            element = (int) o / num;
            getSet().add(element);
        }
    }

    // Метод, который получает на вход Integer и если такое значение есть в коллекции, удаляет его
    public void delete(Integer num) {
        getSet().remove(num);
    }

    @Override
    public String toString() {
        return String.valueOf(getSet());
    }
}
