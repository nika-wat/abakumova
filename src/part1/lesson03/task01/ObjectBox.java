package part1.lesson03.task01;

import java.util.TreeSet;

public class ObjectBox {
    private TreeSet<Object> set = new TreeSet<>();

    public ObjectBox(int[] number) {
        for (int value : number) {
            this.set.add(value);
        }
    }

    public TreeSet<Object> getSet() {
        return set;
    }

    // Метод, добавляющий объект в коллекцию
    public void addObject(Integer object) {
        set.add(object);
    }

    // Метод, проверяющий наличие объекта в коллекции и при наличии удаляющий его
    public void deleteObject(Object object) {
        set.remove(object);
    }

    // Метод, выводящий содержимое коллекции в строку
    public void dump() {
        System.out.println(set);
    }
}
