package part1.lesson05;

public class MyException extends Exception{

    public MyException(String message){
        super(message);
    }
}
