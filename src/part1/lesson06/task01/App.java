package part1.lesson06.task01;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class App {
    public static void main(String[] args) {
        SortedSet<String> set = new TreeSet<>();
        try (FileInputStream fis = new FileInputStream("input.txt")){
            Scanner scanner = new Scanner(fis);
            while (scanner.hasNext()) {
                set.add(scanner.next().toLowerCase());
            }
            Files.write(Paths.get("output.txt"), set);
            System.out.println(set);
            } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
