package part1.lesson06.task02;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class App {
    public static void main(String[] args) throws IOException {
        String path = "MyFiles"; // Название каталога
        int n = 3; // Количество файлов
        int size = 5; // Размер файла
        int n4 = (int) (1 + Math.random() * 1000); // Общее количество слов
        String[] words = GenRandomWords.generateRandomWords(n4); // Генерация массива слов 1<=n4<=1000
        int n1 = (int) (1 + Math.random() * 15); // Предложение состоит из 1<=n1<=15 слов
        ArrayList sentences = GenRandomSentences.generateRandomSentences(n1, words);
        System.out.println(sentences.toString());
        int probability = 1; // Вероятность вхождения одного из слов массива в предложение
//        getFiles(path, n, size, words, probability);
        Files.write(Paths.get("files.txt"), Collections.singleton(sentences.toString()));
    }

        private static void getFiles(String path, int n, int size, String[] words, int probability){
        ByteArrayOutputStream bos = new ByteArrayOutputStream(size);
        byte[] buffer = Arrays.toString(words).getBytes();
        try {
            bos.write(buffer);
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        // Вывод в консоль посимвольно
        byte[] array = bos.toByteArray();
        for (byte b: array) {
            System.out.print((char)b);
        }
    }

}
