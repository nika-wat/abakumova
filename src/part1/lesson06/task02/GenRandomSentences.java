package part1.lesson06.task02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class GenRandomSentences {

    static ArrayList generateRandomSentences(int numberOfSentences, String[]words) {
        ArrayList<ArrayList> randomSentences = new ArrayList<>();
        Random random = new Random();
        for(int i = 0; i < numberOfSentences; i++)
        {
            ArrayList sentence = new ArrayList<>();
            for(int j = 0; j < (random.nextInt(15)+1); j++)
            {
                sentence.add(words[random.nextInt(15)+1]);
            }
            randomSentences.add(sentence);
        }
        return randomSentences;
    }
}
