package part1.lesson06.task02;

import java.util.Random;

public class GenRandomWords {
    static String[] generateRandomWords(int numberOfWords)
    {
        String[] randomWords = new String[numberOfWords];
        Random random = new Random();
        for(int i = 0; i < numberOfWords; i++)
        {
            char[] word = new char[random.nextInt(15)+1]; // Слово состоит из 1<=n2<=15 латинских букв
            for(int j = 0; j < word.length; j++)
            {
                word[j] = (char)('a' + random.nextInt(26));
            }
            randomWords[i] = new String(word);
        }
        return randomWords;
    }
}
