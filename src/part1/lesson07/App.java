package part1.lesson07;

import java.util.concurrent.*;

public class App {
    public static void main(String[] args) {
        int[] mass = Massiv.mass();
        int num;
        Math math = new Math();
        ExecutorService service = Executors.newFixedThreadPool(5);
        for (int value : mass) {
            num = value;
            int finalNum = num;
            Callable<Integer> task = () -> math.factorial(finalNum);
            service.submit(task);
        }
        service.shutdown();
    }
}
