package part1.lesson07;

import java.util.Arrays;

public class Massiv {

    private static final int length = 20; // Длина массива mass

    public static int[] mass() {
        int[] mass = new int[length];
        for (int i = 0; i < length; i++) {
            mass[i] = (int) (java.lang.Math.random() * 30 + 1); // Заполняем mass числами от 1 до 30
        }
        System.out.println(Arrays.toString(mass));
        return mass;
    }
}
