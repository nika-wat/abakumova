package part1.lesson07;

import java.math.BigInteger;

public class Math {

    // Вычисление факториала
    public static Integer factorial(int number) {
        BigInteger result = BigInteger.ONE;
        for (int i = 2; i <= number ; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        System.out.println(number + "! = " + result);
        return 1;
    }
}
