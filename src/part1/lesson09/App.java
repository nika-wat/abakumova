package part1.lesson09;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;

public class App {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        String fileName = "./src/part1/lesson09/SomeClass.java";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
             OutputStream outputStream = new FileOutputStream(fileName)) {
            while (true) {
                String data = reader.readLine();
                if (data.equals("")) {
                    outputStream.write(data.getBytes());
                    break;
                } else {
                    outputStream.write((data + "\n").getBytes());
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        JavaCompiler jc = ToolProvider.getSystemJavaCompiler();
        jc.run(null, null, null, fileName);
        ClassLoader cl = new MyClassLoader();
        Class<?> myClass = cl.loadClass("part1.lesson09.SomeClass"); // ClassNotFoundException: SomeClass
        Worker worker = (Worker) myClass.newInstance();
        worker.doWork();
    }
}
