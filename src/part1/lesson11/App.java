package part1.lesson11;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    public static void main(String[] args) {
        int[] mass = Massiv.mass();
        int num;
        Math math = new Math();
        Demo demo = () -> 5;
        ExecutorService service = Executors.newFixedThreadPool(demo.getValue());
        for (int value : mass) {
            num = value;
            int finalNum = num;
            Callable<Integer> task = () -> math.factorial(finalNum);
            service.submit(task);
        }
        service.shutdown();
    }
}
