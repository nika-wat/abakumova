package part1.lesson11;

import java.util.Arrays;

public class Massiv {

    public static int[] mass() {
        Demo demo;
        demo = () -> 20; // Длина массива mass
        int[] mass = new int[demo.getValue()];
        for (int i = 0; i < mass.length; i++) {
            demo = () -> (int) (java.lang.Math.random() * 30 + 1);
            mass[i] = demo.getValue(); // Заполняем mass числами от 1 до 30
        }
        System.out.println(Arrays.toString(mass));
        return mass;
    }
}
