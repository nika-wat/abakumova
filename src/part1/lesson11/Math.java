package part1.lesson11;

import java.math.BigInteger;

public class Math {

    // Вычисление факториала
    public static Integer factorial(int number) {
        NumericFunc factorial = (n) -> {
            BigInteger result = BigInteger.ONE;
            for (int i = 1; i <= n; i++)
                result = result.multiply(BigInteger.valueOf(i));
            return result;
        };
        System.out.println(number + "! = " + factorial.func(number));
        return 1;
    }
}
