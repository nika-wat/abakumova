package part1.lesson11;

import java.math.BigInteger;

public interface NumericFunc {
    BigInteger func(int n);
}
