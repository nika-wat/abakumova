package part1.lesson15.ConnectionManager;

import org.apache.log4j.Level;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static part1.lesson15.Main.logger;

public class ConnectionManagerJdbcImpl implements ConnectionManager {
    private static ConnectionManager connectionManager;

    private ConnectionManagerJdbcImpl() {
    }

    public static ConnectionManager getInstance() {
        if (connectionManager == null) {
            connectionManager = new ConnectionManagerJdbcImpl();
        }
        return connectionManager;
    }

    @Override
    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/mydatabase",
                    "postgres",
                    "qwerty");
        } catch (ClassNotFoundException | SQLException e) {
            logger.log(Level.ERROR,"Соединение не установлено", e);
        }
        return connection;
    }
}
