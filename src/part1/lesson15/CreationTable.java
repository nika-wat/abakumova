package part1.lesson15;

import org.apache.log4j.Level;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import static part1.lesson15.Main.logger;

public class CreationTable {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/mydatabase",
                "postgres", "qwerty");
             Statement statement = connection.createStatement()) {
            statement.execute("--Database: mydatabase\n"
                    + "DROP TABLE IF EXISTS LOGS CASCADE;"
                    + "CREATE TABLE LOGS (\n"
                    + "    ID varchar(20) NOT NULL,\n"
                    + "    DATE DATE NOT NULL,\n"
                    + "    LOG_LEVEL varchar(10) NOT NULL,\n"
                    + "    MESSAGE varchar(1000) NOT NULL,\n"
                    + "    EXCEPTION varchar(255) NOT NULL);"
                    + "\n"
                    + "DROP TABLE IF EXISTS users CASCADE;"
                    + "CREATE TABLE users (\n"
                    + "    id bigserial primary key,\n"
                    + "    name varchar(255) NOT NULL,\n"
                    + "    birthday varchar(255),\n"
                    + "    login_ID varchar(255) NOT NULL,\n"
                    + "    city varchar(255),\n"
                    + "    email varchar(255) NOT NULL,\n"
                    + "    description varchar(255));"
                    + "\n"
                    + "INSERT INTO users (name, birthday, login_ID, city, email, description)\n"
                    + "VALUES\n"
                    + "   ('Антон', '14/03/1954', 'anton1954', 'Казань', 'ant@mail.ru', 'Новичок'),\n"
                    + "   ('Мария', '12/01/1985', 'mariya-mariya', 'Самара', 'varmar@mail.ru', 'Говорунья'),\n"
                    + "   ('Марат', '01/03/1994', 'maratik', 'Москва', 'marat@mail.ru', 'Элита'),\n"
                    + "   ('Дина', '24/07/1989', 'dinochka', 'Казань', 'dina@mail.ru', 'Новичок'),\n"
                    + "   ('Петр', '19/04/2000', 'petya', 'Томск', 'petr22@mail.ru', 'Любитель'),\n"
                    + "   ('Амир', '13/02/2015', 'amirSh', 'Казань', 'amirSh@mail.ru', 'Новичок');"

                    + "DROP TABLE IF EXISTS role CASCADE;"
                    + "CREATE TABLE role (\n"
                    + "    id bigserial primary key,\n"
                    + "    name varchar(255) NOT NULL,\n"
                    + "    description varchar(255));"
                    + "\n"
                    + "INSERT INTO role (name, description)\n"
                    + "VALUES\n"
                    + "   ('Administration', 'Директор'),\n"
                    + "   ('Administration', 'Секретарь'),\n"
                    + "   ('Billing', 'Бухгалтер'),\n"
                    + "   ('Billing', 'Кассир'),\n"
                    + "   ('Clients', 'Покупатель'),\n"
                    + "   ('Clients', 'Клиент');"

                    + "DROP TABLE IF EXISTS users_role;"
                    + "CREATE TABLE users_role (\n"
                    + "    id bigserial primary key,\n"
                    + "    users_id bigserial NOT NULL references users(id),\n"
                    + "    role_id bigserial NOT NULL references role(id));"
                    + "\n"
                    + "INSERT INTO users_role (users_id, role_id)\n"
                    + "VALUES\n"
                    + "   (1, 1),\n"
                    + "   (2, 3),\n"
                    + "   (3, 5),\n"
                    + "   (4, 2),\n"
                    + "   (5, 6),\n"
                    + "   (6, 4);");
            logger.log(Level.INFO, "Создание таблиц");
        }
    }
}
