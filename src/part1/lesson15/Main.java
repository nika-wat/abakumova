package part1.lesson15;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import part1.lesson15.dao.MyDBDao;
import part1.lesson15.dao.MyDBDaoJdbcImpl;
import part1.lesson15.pojo.Users;

public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        MyDBDao myDBDao = new MyDBDaoJdbcImpl();
        Users user = new Users(null, "Давид", null, "david88", null,
                "david88@mail.ru", null);
        myDBDao.insertUserParam(user);
        myDBDao.batchProc();
        System.out.println(myDBDao.getUser("Давид", "david88"));
        myDBDao.savepoint();
        myDBDao.savepointA();
    }
}

