package part1.lesson15.dao;

import part1.lesson15.pojo.Users;

public interface MyDBDao {
    void insertUserParam(Users user);

    void batchProc();

    Users getUser(String name, String login_ID);

    void savepoint();

    void savepointA();
}
