package part1.lesson15.dao;

import org.apache.log4j.Level;
import org.postgresql.util.PSQLException;
import part1.lesson15.ConnectionManager.ConnectionManager;
import part1.lesson15.ConnectionManager.ConnectionManagerJdbcImpl;
import part1.lesson15.pojo.Users;

import static part1.lesson15.Main.logger;

import java.sql.*;

public class MyDBDaoJdbcImpl implements MyDBDao {
    private static ConnectionManager connectionManager =
            ConnectionManagerJdbcImpl.getInstance();

    @Override
    public void insertUserParam(Users user) {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO users VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getBirthday());
            preparedStatement.setString(3, user.getLogin_ID());
            preparedStatement.setString(4, user.getCity());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setString(6, user.getDescription());
            System.out.println(preparedStatement.executeUpdate());
        } catch (SQLException e) {
            logger.log(Level.ERROR, "Ошибка в insertUserParam", e);
        }
    }

    @Override
    public void batchProc() {
        try (Connection connection = connectionManager.getConnection()) {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            String SQL = "INSERT INTO users VALUES (DEFAULT, 'Марина', null , 'mari55', null , 'mari55@mail.ru'," +
                    "null)";
            statement.addBatch(SQL);
            SQL = "INSERT INTO role VALUES (DEFAULT, 'Administration', null)";
            statement.addBatch(SQL);
            statement.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            logger.log(Level.ERROR, "Ошибка в batchProc", e);
        }
    }

    @Override
    public Users getUser(String name, String login_ID) {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM users WHERE name = ? AND login_ID = ?");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, login_ID);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Users user = new Users(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7));
                return user;
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, "Ошибка в getUser", e);
        }
        return null;
    }

    @Override
    public void savepoint() {
        try (Connection connection = connectionManager.getConnection()) {
            connection.setAutoCommit(false);
            // 1 запись
            String SQL = "INSERT INTO users VALUES (DEFAULT, 'Dарина', null , 'dari000', null , 'dari000@mail.ru'," +
                    "null)";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.executeUpdate();
            // Создание Savepoint
            Savepoint savepoint = connection.setSavepoint("SAVEPOINT");
            // 2 запись
            SQL = "INSERT INTO role VALUES (DEFAULT, 'Rollback', 'Клиент')";
            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.executeUpdate();
            // Rollback к savepoint
            connection.rollback(savepoint);
            // Commit транзакции
            connection.commit();
        } catch (SQLException e) {
            logger.log(Level.ERROR, "Ошибка в savepoint", e);
        }
    }

    @Override
    public void savepointA() {
        try (Connection connection = connectionManager.getConnection()) {
            connection.setAutoCommit(false);
            // Создание Savepoint
            Savepoint savepointA = connection.setSavepoint("SAVEPOINT_A");
            try {
                // 1 запись
                String SQL = "INSERT INTO users VALUES (DEFAULT, 'Максим', null , 'max', null , 'max@mail.ru'," +
                        "null)";
                PreparedStatement preparedStatement = connection.prepareStatement(SQL);
                preparedStatement.executeUpdate();
                // 2 запись (ошибка: запись "null" в "not-null" поле)
                SQL = "INSERT INTO users VALUES (null, null, null , null, null , null, null)";
                preparedStatement = connection.prepareStatement(SQL);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (PSQLException e) {
                connection.rollback(savepointA);
                logger.log(Level.ERROR, "rollback в savepointA", e);
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, "Ошибка в в savepointA", e);
        }
    }
}

