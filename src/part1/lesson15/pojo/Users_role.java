package part1.lesson15.pojo;

public class Users_role {
    private Integer id;
    private Integer users_id;
    private Integer role_id;

    public Users_role(Integer id, Integer users_id, Integer role_id) {
        this.id = id;
        this.users_id = users_id;
        this.role_id = role_id;
    }

    public Users_role() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Integer users_id) {
        this.users_id = users_id;
    }

    public Integer getRole_id() {
        return role_id;
    }

    public void setRole_id(Integer role_id) {
        this.role_id = role_id;
    }

    @Override
    public String toString() {
        return "Users_role{" +
                "id=" + id +
                ", users_id=" + users_id +
                ", role_id=" + role_id +
                '}';
    }
}
