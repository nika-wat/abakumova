package part1.lesson25;

import java.util.TreeSet;

public interface Box {
    TreeSet<Object> getSet();
    void addObject(Integer object);
    void deleteObject(Object object);
    void dump();
}
