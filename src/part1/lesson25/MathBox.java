package part1.lesson25;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class MathBox implements Box {
    private TreeSet<Object> set = new TreeSet<>();

    public MathBox(int[] number) {
        for (int value : number) {
            this.set.add(value);
        }
    }

    // Метод, возвращающий сумму всех элементов коллекции
    public int summator() {
        int summ = 0;
        for (Object o : getSet()) {
            summ += (int) o;
        }
        return summ;
    }

    // Метод, выполняющий поочередное деление всех хранящихся в объекте элементов на делитель
    // Хранящиеся в объекте данные полностью заменяются результатами деления
    public void splitter(Integer num) {
        List list = new ArrayList(getSet());
        getSet().clear();
        int element;
        for (Object o : list) {
            element = (int) o / num;
            getSet().add(element);
        }
    }

    // Метод, который получает на вход Integer и если такое значение есть в коллекции, удаляет его
    public void delete(Integer num) {
        getSet().remove(num);
    }

    @Override
    public String toString() {
        return String.valueOf(getSet());
    }

    @Override
    public TreeSet<Object> getSet() {
        return set;
    }

    @Override
    public void addObject(Integer object) {
        System.out.println("Коллекция MathBox после добавления элемента " + object);
        set.add(object);
    }

    @Override
    public void deleteObject(Object object) {
        System.out.println("Коллекция MathBox после удаления элемента " + object);
        set.remove(object);
    }

    @Override
    public void dump() {
        System.out.println("Коллекция MathBox:");
        System.out.println(set);
    }
}
