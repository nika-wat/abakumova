package part1.lesson25;

import java.util.TreeSet;

public class ObjectBox implements Box {
    private TreeSet<Object> set = new TreeSet<>();

    public ObjectBox(int[] number) {
        for (int value : number) {
            this.set.add(value + 1);
        }
    }

    @Override
    public TreeSet<Object> getSet() {
        return set;
    }

    // Метод, добавляющий объект в коллекцию
    @Override
    public void addObject(Integer object) {
        System.out.println("Коллекция ObjectBox после добавления элемента " + object);
        set.add(object);
    }

    // Метод, проверяющий наличие объекта в коллекции и при наличии удаляющий его
    @Override
    public void deleteObject(Object object) {
        System.out.println("Коллекция ObjectBox после удаления элемента " + object);
        set.remove(object);
    }

    // Метод, выводящий содержимое коллекции в строку
    @Override
    public void dump() {
        System.out.println("Коллекция ObjectBox:");
        System.out.println(set);
    }

    @Override
    public String toString() {
        return String.valueOf(getSet());
    }
}
