package part1.lesson26;

public class Animal {

    private String nickname;
    private Person owner;
    private int weight;

    public Animal(String nickname, Person owner, int weight) {
        this.nickname = nickname;
        this.owner = owner;
        this.weight = weight;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Person getOwner() {
        return owner;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "nickname='" + this.getNickname() + '\'' +
                ", owner=" + owner +
                ", weight=" + weight +
                '}';
    }
}
