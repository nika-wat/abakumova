package part1.lesson26;

public class AnimalDecorator extends Animal {
    private Animal customAnimal;
    public AnimalDecorator (Animal customAnimal)
    {
        super(customAnimal.getNickname(), customAnimal.getOwner(), customAnimal.getWeight());
        this.customAnimal = customAnimal;
    }

    @Override
    public String getNickname() {
        return "Животное по кличке " + customAnimal.getNickname();
    }

    @Override
    public void setNickname(String nickname) {
        customAnimal.setNickname(nickname);
    }
}
