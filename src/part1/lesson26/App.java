package part1.lesson26;

import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        Map<String, Animal> map = new HashMap<>();
        Map<String, HashSet<Animal>> nicknames = new HashMap<>();
        MyAnimals ma = new MyAnimals(map, nicknames);
        Person person1 = new Person("Ефим", 30, Sex.MAN);
        Person person2 = new Person("Елена", 25, Sex.WOMAN);
        Person person3 = new Person("Михаил", 45, Sex.MAN);
        Person person4 = new Person("Вера", 25, Sex.WOMAN);
        Animal animal1 = new Animal("Борис", person1, 10);
        Animal animal2 = new Animal("Дайя", person2, 15);
        Animal animal3 = new Animal("Дайя", person3, 5);
        Animal animal4 = new Animal("Алиса", person4, 7);
        Animal animalDecorator = new AnimalDecorator (animal4);

        // Добавление животных в общий список
        ma.addAnimal(animal1);
        ma.addAnimal(animal2);
        ma.addAnimal(animal3);
        ma.addAnimal(animalDecorator);
//        ma.addAnimal(animal1); // Добавление дубликата для вывода исключения
        System.out.println(Arrays.asList(ma.getAnimalsWithUUID()));

        // Изменение имени животного по его идентификатору
        Scanner in = new Scanner(System.in);
        System.out.println("Введите идентификатор животного, у которого нужно поменять имя:");
        String uniqueKey = in.next();
        ma.renameAnimal(uniqueKey);
        System.out.println(Arrays.asList(ma.getAnimalsWithUUID()));

        // Поиск животного по его кличке
        System.out.println("Введите кличку искомого животного:");
        String nickname = in.next();
        System.out.println(ma.search(nickname));

        // Сортировка списка животных
        ma.sort();
        System.out.println("Вывод списка животных в отсортированном порядке:");
        System.out.println(Arrays.asList(ma.getAnimalsWithUUID()));
    }
}
