package part1.lesson26;

import java.util.*;

public class MyAnimals {
    private Map<String, Animal> animalsWithUUID;
    private Map<String, HashSet<Animal>> myNicknames;

    public MyAnimals(Map<String, Animal> animalsWithUUID, Map<String, HashSet<Animal>> myNicknames) {
        this.animalsWithUUID = animalsWithUUID;
        this.myNicknames = myNicknames;
    }

    public Map<String, Animal> getAnimalsWithUUID() {
        return animalsWithUUID;
    }

    public void addAnimal(Animal animal) throws Exception {
        if (animalsWithUUID.containsValue(animal)) {
            throw new MyException("Такое животное уже добавлено!");
        }
        String uniqueKey = UUID.randomUUID().toString();
        animalsWithUUID.put(uniqueKey, animal);
        if (!myNicknames.containsKey(animal.getNickname())) {
            myNicknames.put(animal.getNickname(), new HashSet<>());
        }
        myNicknames.get(animal.getNickname()).add(animal);
    }

    public Set<Animal> search(String nickname) {
        if (!myNicknames.containsKey(nickname)) return null;
        return myNicknames.get(nickname);
    }

    public void sort() {
        List<Animal> sortedAnimals = new ArrayList<Animal>(animalsWithUUID.values());
        sortedAnimals.sort(new MyComparator());
    }

    public void renameAnimal(String uniqueKey) {
        if (!animalsWithUUID.containsKey(uniqueKey)) return;
        Animal animal = animalsWithUUID.get(uniqueKey);
        System.out.println("Введите новое имя:");
        Scanner in = new Scanner(System.in);
        String newName = in.next();
        myNicknames.get(animal.getNickname()).remove(animal);
        if (!myNicknames.containsKey(newName)) {
            myNicknames.put(newName, new HashSet<>());
        }
        myNicknames.get(newName).add(animal);
        animalsWithUUID.get(uniqueKey).setNickname(newName);
    }
}


