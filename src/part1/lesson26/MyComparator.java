package part1.lesson26;

import java.util.Comparator;

public class MyComparator implements Comparator<Animal> {

    @Override
    // Поля для сортировки – хозяин, кличка животного, вес
    public int compare(Animal a, Animal b) {
        int result = a.getOwner().getName().compareTo(b.getOwner().getName()); // Сравниваем по имени хозяина
        if (result != 0) {
            return result;
        }
        result = a.getOwner().getAge() - b.getOwner().getAge(); // Сравниваем по возрасту хозяина
        if (result != 0) {
            return result;
        }
        result = a.getNickname().compareTo(b.getNickname()); // Если хозяева равны, сравниваем по кличке
        if (result != 0) {
            return result;
        }
        result = a.getWeight() - b.getWeight(); // Если клички равны, сравниваем по весу
        return result;
    }
}

