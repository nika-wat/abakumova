package part1.lesson26;

public class MyException extends Exception{

    public MyException(String message){
        super(message);
    }
}
