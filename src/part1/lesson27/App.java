package part1.lesson27;

import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        Map<String, Animal> map = new HashMap<>();
        Map<String, HashSet<Animal>> nicknames = new HashMap<>();
        MyAnimals ma = new MyAnimals(map, nicknames);
        Person person1 = new Person("Ефим", 30, Sex.MAN);
        Person person2 = new Person("Елена", 25, Sex.WOMAN);
        Person person3 = new Person("Михаил", 45, Sex.MAN);
        Animal animal1 = new Animal("Борис", person1, 10);
        Animal animal2 = new Animal("Дайя", person2, 15);
        Animal animal3 = new Animal("Алиса", person3, 5);

        // Добавление животных в общий список
        ma.addAnimal(animal1);
        ma.addAnimal(animal2);
        ma.addAnimal(animal3);

        // Сортировка списка животных
        Context context = new Context(new MyComparatorByOwner());
        System.out.println("Вывод списка животных, отсортированного по хозяину:");
        context.compare(ma);
        context = new Context(new MyComparatorByName());
        System.out.println("Вывод списка животных, отсортированного по кличке:");
        context.compare(ma);
    }
}
