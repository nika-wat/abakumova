package part1.lesson27;

public class Context {
    private MyComparator strategy;
    public Context(MyComparator strategy){
        this.strategy = strategy;
    }
    public void compare(MyAnimals ma){
        ma.sort(strategy);
    }
}
