package part1.lesson27;

import java.util.*;

public class MyAnimals {
    private Map<String, Animal> animalsWithUUID;
    private Map<String, HashSet<Animal>> myNicknames;

    public MyAnimals(Map<String, Animal> animalsWithUUID, Map<String, HashSet<Animal>> myNicknames) {
        this.animalsWithUUID = animalsWithUUID;
        this.myNicknames = myNicknames;
    }

    public Map<String, Animal> getAnimalsWithUUID() {
        return animalsWithUUID;
    }

    public void addAnimal(Animal animal) throws Exception {
        if (animalsWithUUID.containsValue(animal)) {
            throw new MyException("Такое животное уже добавлено!");
        }
        String uniqueKey = UUID.randomUUID().toString();
        animalsWithUUID.put(uniqueKey, animal);
        if (!myNicknames.containsKey(animal.getNickname())) {
            myNicknames.put(animal.getNickname(), new HashSet<>());
        }
        myNicknames.get(animal.getNickname()).add(animal);
    }

    public void sort(MyComparator strategy) {
        List<Animal> sortedAnimals = new ArrayList<Animal>(animalsWithUUID.values());
        sortedAnimals.sort(strategy);
        System.out.println(sortedAnimals);
    }
}


