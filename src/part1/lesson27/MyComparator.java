package part1.lesson27;

import java.util.Comparator;

public interface MyComparator extends Comparator<Animal> {
    @Override
    int compare(Animal a, Animal b);
}
