package part1.lesson27;

public class MyComparatorByName implements MyComparator {

    @Override
    // Поля для сортировки – хозяин, кличка животного, вес
    public int compare(Animal a, Animal b) {
        int result = a.getNickname().compareTo(b.getNickname()); // Сравниваем по кличке
        if (result != 0) {
            return result;
        }
        result = a.getWeight() - b.getWeight(); // Если клички равны, сравниваем по весу
        return result;
    }
}
