package part1.lesson27;

public class MyComparatorByOwner implements MyComparator {

    @Override
    // Поля для сортировки – хозяин, кличка животного, вес
    public int compare(Animal a, Animal b) {
        int result = a.getOwner().getName().compareTo(b.getOwner().getName()); // Сравниваем по имени хозяина
        if (result != 0) {
            return result;
        }
        result = a.getOwner().getAge() - b.getOwner().getAge(); // Сравниваем по возрасту хозяина
        return result;
    }
}
