package part1.lesson27;

public class MyException extends Exception{

    public MyException(String message){
        super(message);
    }
}
